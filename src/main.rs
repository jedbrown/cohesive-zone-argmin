use argmin::{
    core::{observers::ObserverMode, Error, Executor, Gradient, Hessian, CostFunction},
    solver::{newton::{Newton, NewtonCG}, linesearch::MoreThuenteLineSearch},
};
use argmin_observer_slog::SlogLogger;
//use num::{Float, /*FromPrimitive*/};
use std::{/*iter::Sum, ops::AddAssign, */f64::consts::E /*ops::{Mul,Div}*/};
use clap::{Parser, Args, ValueEnum};
//use ndarray::{array, Array1, Array2}; <- required for bfgs?

//Cohesive Zone Model Function
#[allow(non_snake_case)]
pub fn CZM(p: &[&f64; 5]) -> f64
{
    let x  : &f64 = p[0];
    let c1 : &f64 = p[1];
    let c2 : &f64 = p[2];

    let result = (c1/c2) * x * E.powf(1.0 - (x/c2));
    result
}

//Derivative of CZM
#[allow(non_snake_case)]
pub fn dCZM(p: &[&f64; 5]) -> f64
{
    let x  : &f64 = p[0];
    let c1 : &f64 = p[1];
    let c2 : &f64 = p[2];

    let result = (c1/c2) * E.powf(1.0 - (x/c2)) - ((c1*x)/(c2.powf(2.0))*E.powf(1.0 - (x/c2)));
    result
}

//Double Derivative of CZM
#[allow(non_snake_case)]
pub fn ddCZM(p: &[&f64; 5]) -> f64
{
    let x  : &f64 = p[0];
    let c1 : &f64 = p[1];
    let c2 : &f64 = p[2];

    let result = -(2.0*c1*E.powf(1.0 - (x/c2)))/(c2.powf(2.0)) + (c1*E.powf(1.0 - (x/c2))*x)/(c2.powf(3.0));
    result
}

//Material response function
pub fn material_response(p: &[&f64; 5]) -> f64
{
    let x  : &f64 = p[0];
    let k  : &f64 = p[3];
    let dh : &f64 = p[4];

    let result = -k*(x) + k*(dh);
    result
}

//Derivative of Material response function
pub fn dmaterial_response(p: &[&f64; 5]) -> f64
{
    let k  : &f64 = p[3];

    -k
}

//Described as P'-P in paper this function finds the point the material response and CZM intersect
pub fn root_fn(p: &[&f64; 5]) -> f64
{
    CZM(p) - material_response(p)
}

//integral of the root finding function, used to implement the cost function
pub fn iroot_fn(p: &[&f64; 5]) -> f64
{
    let x  : &f64 = p[0];
    let c1 : &f64 = p[1];
    let c2 : &f64 = p[2];
    let k  : &f64 = p[3];
    let dh : &f64 = p[4];

    let result = -c1*E.powf(1.0 - (x/c2) )*(c2 + x) + (k/2.0)*x*(-2.0*dh+x);
    result
}

//derivative of the root finding function
pub fn droot_fn(p: &[&f64; 5]) -> f64
{
    dCZM(p) - dmaterial_response(p)
}

//double derivative of root function function
pub fn ddroot_fn(p: &[&f64; 5]) -> f64
{
    ddCZM(p)
}

struct Opt {
    c1: f64,
    c2: f64,
    k:  f64,
    dh: f64,
}

//Note to self, these traits are named poorly

impl CostFunction for Opt {
    type Param = f64;
    type Output = f64;

    fn cost(&self, p: &Self::Param) -> Result<Self::Output, Error>{
        let param = [p, &self.c1, &self.c2, &self.k, &self.dh];
        Ok(iroot_fn(&param))
    }
}

impl Gradient for Opt {
    type Param = f64;
    type Gradient = f64;

    fn gradient(&self, p: &Self::Param) -> Result<Self::Gradient, Error> {
        let param = [p, &self.c1, &self.c2, &self.k, &self.dh];
        Ok(root_fn(&param))
    }
}

impl Hessian for Opt {
    type Param = f64;
    type Hessian = f64;

    fn hessian(&self, p: &Self::Param) -> Result<Self::Hessian, Error> {
        let param = [p, &self.c1, &self.c2, &self.k, &self.dh];
        let h = droot_fn(&param);
            /*.into_iter()
            .flatten()
            .collect();*/
        Ok(h)
    }
}

//Function to execute the newton solver and log results
fn run_newton (p: &[f64; 5]) -> Result<(), Error> {
    let cost = Opt {
        c1 : p[0], 
        c2 : p[1], 
        k  : p[2], 
        dh : p[3], 
    };

    let init_param: f64 = p[4];

    let solver : Newton<f64> = Newton::new();

    let res = Executor::new(cost, solver)
        .configure(|state| state.param(init_param).max_iters(10))
        .add_observer(SlogLogger::term(), ObserverMode::Always)
        .run()?;

    println!("{res}");
    Ok(())
}

//function o execute the line search and bgfs solver and log results
fn run_newton_cg (p: &[f64; 5]) -> Result<(), Error> {
    let cost = Opt {
        c1 : p[0],
        c2 : p[1],
        k  : p[2],
        dh : p[3],
    };

    let init_param   : f64 = p[4];
    //let init_hessian : f64 = 1.0; <- required for bfgs?

    let linesearch = MoreThuenteLineSearch::new();

    let solver = NewtonCG::new(linesearch);

    let res = Executor::new(cost, solver)
        .configure(|state| state.param(init_param).max_iters(10))
        .add_observer(SlogLogger::term(), ObserverMode::Always)
        .run()?;

    //print result
    println!("{res}");
    Ok(())
}

#[derive(ValueEnum, Debug, Clone)]
enum Algorithm {
    Newton,
    NewtonCG,
}

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct CLI {
    /// Shorthand command to input model constants, space delimited
    #[arg(short, long, num_args(5), value_names(["SIGMA_C", "DELTA_C", "MATERIAL_RESPONSE_K", "DISPLACEMENT_H", "INITAL_GUESS"]), group="list", conflicts_with="struct")] 
    consts: Vec<f64>,

    #[command(flatten)]
    constants: Consts,

    /// Chosen method for solving Model
    #[arg(value_enum, short, long)]
    algorithm: Algorithm,

}

#[derive(Args, Debug)]
#[group(conflicts_with="list", id="struct", requires_all=["sigma_c", "delta_c", "k", "dh", "guess"])]
struct Consts {
    /// Sets sigma constant of CZM
    #[arg(long, value_name = "SIGMA_C", default_value="3.0")]
    sigma_c: f64,

    /// Sets delta constatnt of CZM
    #[arg(long, value_name = "DELTA_C", default_value="2.0")]
    delta_c: f64,

    /// Sets k constatnt of Material Response function
    #[arg(long, value_name = "MATERIAL_RESPONSE_K", default_value="1.0")]
    k: f64,

    /// Sets displacement h constatnt of Material Response function
    #[arg(long, value_name = "DISPLACEMENT_H", default_value="4.0")]
    dh: f64,

    /// Sets the inital guess of the numerial method used to solve for the model
    #[arg(long, value_name = "INITAL_GUESS", default_value="2.8")]
    guess: f64,
}

fn main() {
    //try 3.0, 2.0, 1.0, 4.0!
    let args_wrap = CLI::parse();
    let p = 
    if args_wrap.consts.len() != 0 {
        let args = args_wrap.consts;
        [args[0], args[1], args[2], args[3], args[4]]
    } else {
        let args = args_wrap.constants;
        [args.sigma_c, args.delta_c, args.k, args.dh, args.guess]
    };
    match args_wrap.algorithm {
        Algorithm::Newton => {
            if let Err(ref e) = run_newton(&p) {
                println!("{e}");
                std::process::exit(1);
            }
        },
        Algorithm::NewtonCG => {
            if let Err(ref e) = run_newton_cg(&p) {
                println!("{e}");
                std::process::exit(1);
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand::random;

    #[test]
    fn test_newton() {
        for _ in 1..1501 {
            let v = [random(), random(), random(), random(), random()];
            if let Err(ref e) = run_newton(&v) {
                println!("{e}");
            };
        }
    }

    #[test]
    fn test_newton_cg() {
        for _ in 1..1501 {
            let v = [random(), random(), random(), random(), random()];
            if let Err(ref e) = run_newton_cg(&v) {
                println!("{e}");
            };
        }
    }
}
